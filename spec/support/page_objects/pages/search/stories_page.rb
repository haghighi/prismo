require_relative 'base_page'

class Search::StoriesPage < Search::BasePage
  set_url '/search'
  set_url_matcher %r{\/search}

  sections :stories, ::StoryRowSection, '.story-row'
end
