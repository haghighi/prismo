# frozen_string_literal: true

module Stories
  class EditFlagPage < SitePrism::Page
    set_url '/posts{/id}/flag/edit'
    set_url_matcher %r{\/posts\/[a-zA-Z0-9-]+\/flag\/edit\z}

    section :edit_flag_form, '#edit_flag' do
      element :flag_summary_input, '#flag_summary'
      element :submit_btn, 'input[type="submit"]'

      def submit
        submit_btn.click
      end
    end
  end
end
