# frozen_string_literal: true

module Stories
  class EditPage < SitePrism::Page
    set_url '/posts{/id}/edit'
    set_url_matcher %r{\/posts\/[a-zA-Z0-9-]+(\/edit)?\z}

    element :url_field, 'input#story_url'
    element :title_field, 'input#story_name'
    element :tag_list_field, '.tagify__input'
    element :description_field, 'textarea#story_content_source'
    element :submit_button, '[type="submit"]'
  end
end
