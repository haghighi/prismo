# frozen_string_literal: true

require_relative '../../sections/story_row_section'
require_relative '../../sections/tag_heading_section'

module Tags
  class ShowPage < SitePrism::Page
    set_url '/tags{/name}'
    set_url_matcher %r{\/tags\/\w+\z}

    section :heading, TagHeadingSection, 'h2'
    sections :stories, StoryRowSection, '.story-row'
  end
end
