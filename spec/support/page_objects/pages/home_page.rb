# frozen_string_literal: true

class HomePage < SitePrism::Page
  set_url '/'
  set_url_matcher %r{\/\z}

  section :navbar, ::NavbarSection, '.main-header'
  sections :stories, ::StoryRowSection, '.story-row'
end
