# frozen_string_literal: true

FactoryBot.define do
  factory :follow_request do
    association :follower, factory: :account
    association :following, factory: :account
  end
end
