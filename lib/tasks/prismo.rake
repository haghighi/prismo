# frozen_string_literal: true

namespace :prismo do
  task migrate_to_ap_objects: :environment do
    Story.order(id: :desc).all.each do |story|
      puts "=== Migrating story #{story.id}"
      ap_post = ActivityPubPost.find_or_create_by(uuid: story.uuid) do |p|
        p.uuid = story.uuid
        p.account = story.account
        p.url = story.url
        p.name = story.title
        p.content_source = story.description
        # p.likes_count = story.likes_count
        p.created_at = story.created_at
        p.updated_at = story.updated_at
        p.replies_count = story.comments_count
        p.domain = story.domain
        p.url_meta_id = story.url_meta_id
        p.settings.scrapped_at = story.scrapped_at
        p.uri = story.uri
        p.url_domain = story.url_domain
        p.group_id = story.group_id
        p.removed = story.removed
        p.modified_at = story.modified_at
        p.modified_count = story.modified_count
      end

      ap_post.likes.destroy_all
      story.likes.order(id: :desc).each do |like|
        Like.create!(
          likeable: ap_post,
          account: like.account,
          created_at: like.created_at,
          updated_at: like.updated_at
        )
        print '~'
      end

      ap_post.refresh_likes_count

      puts
    end

    Comment.order(depth_cached: :asc).each do |comment|
      puts "=== Migrating comment #{comment.id}"

      ap_comment = ActivityPubComment.find_or_create_by!(uuid: comment.uuid) do |c|
        c.uuid = comment.uuid
        c.account = comment.account
        c.content_source = comment.body
        c.likes_count = comment.likes_count
        c.created_at = comment.created_at
        c.updated_at = comment.updated_at
        c.domain = comment.domain
        c.uri = comment.uri
        c.removed = comment.removed
        c.modified_at = comment.modified_at
        c.modified_count = comment.modified_count

        parent = comment.parent_id.present? ? comment.parent : comment.story
        c.parent = ActivityPubObject.find_by(uuid: parent.uuid)
      end

      ap_comment.cache_body
      ap_comment.cache_depth
      ap_comment.cache_root

      ap_comment.likes.destroy_all
      comment.likes.order(id: :desc).each do |like|
        Like.create!(
          likeable: ap_comment,
          account: like.account,
          created_at: like.created_at,
          updated_at: like.updated_at
        )
        print '~'
      end

      ap_comment.refresh_likes_count

      puts
    end

    puts '=== Refreshing descendants count for posts'
    ActivityPubPost.all.each do |post|
      post.update(children_count: post.descendants.count)
      print '.'
    end

    puts '=== Removing flags and notifications'
    Flag.destroy_all
    Notification.destroy_all

    puts 'Finished!'
  end

  desc 'TODO'
  task seed_comments: :environment do
    # rubocop:disable Metrics/LineLength
    bodies = [
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend mi ut massa dictum egestas. Etiam rutrum tortor vel orci vehicula blandit. Cras placerat leo dolor, nec euismod ipsum fringilla at. Nullam auctor maximus fringilla. Integer et lorem quis elit scelerisque suscipit in in leo. Vivamus pretium elit sed ipsum laoreet, efficitur imperdiet neque facilisis. Nam finibus ante id aliquet ullamcorper. Donec eget ex nec est volutpat finibus. Cras ornare nulla vel tellus iaculis tristique. Nunc dictum vel urna quis consectetur. Donec malesuada felis quis purus bibendum maximus. Duis dignissim orci nec leo sollicitudin, at laoreet augue ultricies.',
      'Duis arcu enim, interdum at lorem vel, dignissim eleifend augue. Aliquam sed commodo quam. Integer lorem massa, aliquet id fringilla ut, elementum a ligula. Quisque nec condimentum urna. Proin sed purus eget lacus gravida bibendum. Nam ac est et felis interdum lobortis. Ut eu enim gravida, blandit justo at, tempus felis. In eget tristique justo. Etiam viverra, leo quis pharetra eleifend, est turpis auctor urna, id euismod diam velit in velit. Nulla tristique turpis eu lectus cursus, quis pharetra felis laoreet. Integer sit amet ultricies metus, non auctor ligula.',
      'In dignissim vestibulum neque, quis fringilla velit scelerisque nec. Pellentesque nec placerat massa. Nunc eu lorem tortor. Ut blandit massa ac lorem lacinia tempus. Phasellus eu hendrerit nisi. Nulla gravida sagittis feugiat. Aliquam ultricies ultrices nisi sed consequat. Mauris a arcu eget ligula hendrerit venenatis.'
    ].freeze
    # rubocop:enable Metrics/LineLength

    100.times do |i|
      account = Account.order('RANDOM()').first
      parent = Comment.order('RANDOM()').first
      story = parent.story

      comment = parent.children.create! account: account,
                                        story: story,
                                        body: bodies.sample

      comment.cache_depth
      comment.cache_body

      puts i
    end
  end

  desc 'TODO'
  task cast_empty_likes: :environment do
    puts 'Enter story ID:'
    story_id = STDIN.gets.chomp
    story = Story.find(story_id)
    puts "Story found: #{story.title}"

    puts 'Number of likes:'
    likes_count = STDIN.gets.chomp.to_i

    story.update(likes_count: story.likes_count + likes_count)

    puts "Likes counter incremented. New value: #{story.reload.likes_count}"
  end

  desc 'Sets up Prismo'
  task setup: :environment do
    puts '=== Creating master group'
    Group.create!(name: 'General', supergroup: true)
    puts 'Done.'

    puts '=== Creating admin account'
    Rake::Task['prismo:create_admin'].invoke
    puts 'Done.'
  end

  desc 'Create admin user'
  task create_admin: :environment do
    require 'io/console'

    puts 'Create a new admin user'
    puts

    print 'Username: '
    username = STDIN.gets.chomp

    print 'Display name: '
    displayname = STDIN.gets.chomp

    print 'E-mail: '
    email = STDIN.gets.chomp

    print 'Password: '
    password = STDIN.noecho(&:gets).chomp
    puts

    account = Account.create!(username: username, display_name: displayname)
    User.create!(
      account: account,
      email: email,
      password: password,
      is_admin: true,
      confirmed_at: Time.now
    )

    puts
    puts 'Admin user created.'
  end

  desc 'Create regular user with account'
  task create_user: :environment do
    require 'io/console'

    puts 'Create a new user'
    puts

    print 'Username: '
    username = STDIN.gets.chomp

    print 'Display name: '
    displayname = STDIN.gets.chomp

    print 'E-mail: '
    email = STDIN.gets.chomp

    print 'Password: '
    password = STDIN.noecho(&:gets).chomp
    puts

    User.create!(
      email: email,
      password: password,
      confirmed_at: Time.now,
      account_attributes: {
        username: username,
        display_name: displayname
      }
    )

    puts
    puts 'User created.'
  end

  task refresh_accounts_karma: :environment do
    puts 'Refreshing accounts karma'

    Account.all.each do |account|
      account.update(
        posts_karma: account.stories.sum(:likes_count),
        comments_karma: account.comments.sum(:likes_count)
      )
      print '.'
    end

    puts
  end
end
