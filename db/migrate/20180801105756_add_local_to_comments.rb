class AddLocalToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :local, :boolean
  end
end
