class AddThumbDataToUrlMeta < ActiveRecord::Migration[5.2]
  def change
    add_column :url_meta, :thumb_data, :jsonb, default: {}
    remove_column :stories, :thumb_data
  end
end
