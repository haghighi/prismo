class CreateActivityPubObjectHierarchies < ActiveRecord::Migration[5.2]
  def change
    create_table :activitypub_object_hierarchies, id: false do |t|
      t.integer :ancestor_id, null: false
      t.integer :descendant_id, null: false
      t.integer :generations, null: false
    end

    add_index :activitypub_object_hierarchies, [:ancestor_id, :descendant_id, :generations],
      unique: true,
      name: "activitypub_comment_anc_desc_idx"

    add_index :activitypub_object_hierarchies, [:descendant_id],
      name: "activitypub_comment_desc_idx"
  end
end
