class RemoveProtocolFromAccounts < ActiveRecord::Migration[5.2]
  def change
    remove_column :accounts, :protocol
  end
end
