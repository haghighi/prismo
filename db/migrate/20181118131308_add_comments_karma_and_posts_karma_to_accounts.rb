class AddCommentsKarmaAndPostsKarmaToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :comments_karma, :integer, default: 0
    add_column :accounts, :posts_karma, :integer, default: 0

    Account.all.map(&:refresh_karma)
  end
end
