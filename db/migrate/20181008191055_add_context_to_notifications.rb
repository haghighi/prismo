class AddContextToNotifications < ActiveRecord::Migration[5.2]
  def change
    add_reference :notifications, :context, polymorphic: true
  end
end
