class AddStoriesCountAndCommentsCountToAccount < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :stories_count, :integer, default: 0
    add_column :accounts, :comments_count, :integer, default: 0
  end
end
