class AddMissingFieldsToAccount < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :followers_url, :string
    add_column :accounts, :following_count, :integer, default: 0
    add_column :accounts, :followers_count, :integer, default: 0
    add_column :accounts, :locked, :boolean, default: false
  end
end
