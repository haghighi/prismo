class CreateActivityPubObjects < ActiveRecord::Migration[5.2]
  def change
    create_table :activitypub_objects do |t|
      t.string :url
      t.string :type
      t.string :name
      t.text :content
      t.text :content_source
      t.references :account, foreign_key: true
      t.references :url_meta, foreign_key: true
      t.references :parent, foreign_key: { to_table: :activitypub_objects }, index: true, on_delete: :cascade
      t.references :root_cached, foreign_key: { to_table: :activitypub_objects }, index: true
      t.integer :likes_count, default: 0
      t.integer :replies_count, default: 0
      t.string :domain
      t.string :uri
      t.string :url_domain
      t.references :group, foreign_key: true
      t.boolean :removed, default: false
      t.datetime :modified_at
      t.integer :modified_count, default: 0
      t.integer :depth_cached, default: 0
      t.integer :children_count, default: 0
      t.uuid :uuid, default: 'public.uuid_generate_v4()'

      t.timestamps
    end
  end
end
