class CreateFlags < ActiveRecord::Migration[5.2]
  def change
    create_table :flags do |t|
      t.references :actor, polymorphic: true
      t.references :flaggable, polymorphic: true
      t.text :summary
      t.boolean :action_taken, default: false

      t.timestamps
    end
  end
end
