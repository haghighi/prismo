# frozen_string_literal: true

class FollowTagButtonPresenter
  include ActionView::Helpers::UrlHelper
  include ActionView::Helpers::TextHelper

  attr_reader :actor, :tag

  def initialize(actor, tag)
    @actor = actor
    @tag = tag
  end

  def following?
    return false if actor.blank?

    actor.following?(tag)
  end

  def icon_class
    following? ? 'fe-user-check' : 'fe-user-plus'
  end

  def label
    following? ? 'Following' : 'Follow'
  end

  def root_class
    following? ? 'btn-secondary' : 'btn-primary'
  end
end
