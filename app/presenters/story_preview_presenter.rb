# frozen_string_literal: true

# @todo Consider moving it to a separate "component pattern" ?
#
class StoryPreviewPresenter < Draper::Decorator
  SUPPORTED_ONEBOX_MATCHERS = [
    Onebox::Engine::VideoOnebox,
    Onebox::Engine::YoutubeOnebox
  ].freeze

  def render
    return if object.url.blank? || !supported_onebox_matcher?

    h.content_tag :div, class: container_class do
      url_preview.to_s.html_safe
    end
  end

  private

  def container_class
    case onebox_matcher
    when Onebox::Engine::YoutubeOnebox.class
      ['video-responsive']
    else
      []
    end
  end

  def url_preview
    @url_preview ||= Onebox.preview(object.url, cache: Rails.cache)
  end

  def supported_onebox_matcher?
    SUPPORTED_ONEBOX_MATCHERS.include? onebox_matcher
  end

  def onebox_matcher
    @matcher ||= Onebox::Matcher.new(object.url).oneboxed
  end
end
