# frozen_string_literal: true

class Stories::CreatePolicy < ActivityPubPostPolicy
  def update_url?
    true
  end
end
