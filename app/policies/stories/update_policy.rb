# frozen_string_literal: true

class Stories::UpdatePolicy < ActivityPubPostPolicy
  # @todo move that to StoryPolicy
  def update_url?
    user.is_admin?
  end
end
