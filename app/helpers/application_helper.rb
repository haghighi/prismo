# frozen_string_literal: true

module ApplicationHelper
  def current_user_setting(setting)
    return unless current_user.present?

    current_user.settings.send(setting.to_sym)
  end
end
