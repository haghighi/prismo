module LayoutsHelper
  def narrow_layout(&block)
    content_tag(:div, class: 'container margin-top--lg') do
      content_tag(:div, class: 'grid justify--center') do
        content_tag(:div, class: 'col--sm-6') do
          yield
        end
      end
    end
  end
end
