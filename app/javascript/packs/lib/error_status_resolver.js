import EventBus from './eventbus'

export default function (status) {
  switch (status) {
    case 401:
      EventBus.addToast({ text: 'You need to be signed in to do this', severity: 'error' })
      break;
    case 403:
      EventBus.addToast({ text: 'You are not authorized to do this', severity: 'error' })
      break;
    case 400:
      break;
    default:
      EventBus.addToast({ text: 'Something went wrong. Please try again later', severity: 'error' })
      break;
  }
}
