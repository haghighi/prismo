export default {
  dispatch(e, payload) {
    var event = new CustomEvent(e, { detail: payload })
    document.body.dispatchEvent(event)
  },

  addEventListener(event, fx) {
    return document.body.addEventListener(event, fx)
  },

  removeEventListener(event, fx) {
    return document.body.removeEventListener(event, fx)
  },

  addToast (payload) {
    this.dispatch('toasts:add', payload)
  }
}
