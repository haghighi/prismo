import BaseController from "./base_controller"
import Popper from 'popper.js'

export default class extends BaseController {
  static targets = ['toggle', 'dropdown']

  connect() {
    this._popper = new Popper(this.toggleTarget, this.dropdownTarget, {
      placement: this.placement,
      eventsEnabled: false
    })

    document.addEventListener('turbolinks:before-cache', this._handleTurbolinksBeforeCache.bind(this))
  }

  disconnect() {
    this.opened = false
    this._popper.destroy()

    document.removeEventListener('turbolinks:before-cache', this._handleTurbolinksBeforeCache)
  }

  toggle (e) {
    e.preventDefault()
    this.opened = !this.dropdownTarget.classList.contains('dropdown--opened')
  }

  hide (e) {
    if (this.element.contains(e.target) == false) {
      this.opened = false
    }
  }

  _handleTurbolinksBeforeCache () {
    this.opened = false
  }

  get placement () {
    return this.data.get('placement') || 'bottom-start'
  }

  set opened (value) {
    if (value) {
      this._popper.enableEventListeners()
      this._popper.update()
      this.dropdownTarget.classList.add('dropdown--opened')
    } else {
      this.dropdownTarget.classList.remove('dropdown--opened')
      this._popper.disableEventListeners()
    }
  }
}
