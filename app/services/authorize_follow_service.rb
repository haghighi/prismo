# frozen_string_literal: true

class AuthorizeFollowService < BaseService
  def call(follower, following, **options)
    if options[:skip_follow_request]
      follow_request = FollowRequest.new(follower: follower,
                                         following: following,
                                         uri: options[:follow_request_uri])
    else
      follow_request = FollowRequest.find_by!(follower: follower,
                                              following: following)
      follow_request.authorize!
    end

    create_notification(follow_request) if follower.remote?

    follow_request
  end

  private

  def create_notification(follow_request)
    ActivityPub::DeliveryJob.perform_later(signed_payload(follow_request),
                                           follow_request.following_id,
                                           follow_request.follower.inbox_url)
  end

  def signed_payload(follow_request)
    Oj.dump(
      ActivityPub::LinkedDataSignature
        .new(payload(follow_request))
        .sign!(follow_request.following)
    )
  end

  def payload(follow_request)
    ActivityPub::AcceptFollowSerializer.new(follow_request,
                                            with_context: true).as_json
  end
end
