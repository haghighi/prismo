# frozen_string_literal: true

class Nodeinfo2Serializer < Granola::Serializer
  include RoutingHelper

  def initialize
    super(nil)
  end

  def data
    {
      version: '2.0',
      software: software,
      protocols: ['activitypub'],
      services: services,
      openRegistrations: Setting.open_registrations,
      usage: usage,
      metadata: metadata
    }
  end

  def software
    {
      name: 'prismo',
      version: Prismo::Version
    }
  end

  def metadata
    {
      baseUrl: root_url,
      name: Setting.site_title,
      description: Setting.site_description,
    }
  end

  def usage
    {
      users: {
        total: User.all.count,
        activeHalfyear: Account.local.active_halfyear.count,
        activeMonth: Account.local.active_month.count
      },

      localPosts: ActivityPubPost.local.count,
      localComments: ActivityPubComment.local.count
    }
  end

  def services
    {
      inbound: [],
      outbound: []
    }
  end
end
