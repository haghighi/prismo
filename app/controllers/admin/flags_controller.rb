# frozen_string_literal: true

module Admin
  class FlagsController < ApplicationController
    layout 'settings'

    def index
      authorize Flag
      @flags = Flag.order(created_at: :desc).page(params[:page])
    end

    def show
      @flag = find_flag
      authorize @flag
    end

    def resolve
      @flag = find_flag
      authorize @flag

      Flags::Resolve.run(flag: @flag)

      redirect_to admin_flag_path(@flag), notice: 'Flag marked as resolved'
    end

    def unresolve
      @flag = find_flag
      authorize @flag

      Flags::Unresolve.run(flag: @flag)

      redirect_to admin_flag_path(@flag), notice: 'Flag marked as unresolved'
    end

    private

    def find_flag
      @find_flag ||= Flag.find(params[:id]).decorate
    end
  end
end
