class ActivityPubComment < ActivityPubObject

  pg_search_scope :search, against: :content,
                           using: { tsearch: { prefix: true } }

  belongs_to :account, counter_cache: :comments_count, optional: true

  def cache_depth
    update_attributes(depth_cached: depth)
  end

  def object_type
    :note
  end

  def cache_depth
    update_attributes(depth_cached: depth)
  end

  def cache_body
    update_attributes(
      content: BodyParser.new(content_source).call
    )
  end

  def object_type
    :note
  end
end
