# frozen_string_literal: true

class UrlMeta < ApplicationRecord
  include UrlMetaThumbUploader[:thumb]
end
